import 'package:aog/widgets/components/logo.dart';
import 'package:aog/widgets/views/main-form.dart';
import 'package:aog/widgets/views/success.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Color _color = Colors.deepPurple;
  var _gasController = new MoneyMaskedTextController();
  var _alcoholController = new MoneyMaskedTextController();
  var _isBusy = false;
  var _isCompleted = false;
  var _hasError = false;
  var _message = "";
  var _complementaryInfo = "Complementary Text";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: new GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: AnimatedContainer(
            duration: Duration(milliseconds: 1200),
            color: _color,
            child: ListView(
              children: <Widget>[
                Logo(),
                _isCompleted
                    ? Success(
                        message: _message,
                        complementaryInfo: _complementaryInfo,
                        onResetForm: resetForm,
                        hasError: _hasError,
                      )
                    : MainForm(
                        alcoholController: _alcoholController,
                        gasController: _gasController,
                        onSubmit: calculate,
                        isBusy: _isBusy,
                      )
              ],
            ),
          ),
        ));
  }

  Future calculate() {
    double alcohol = _alcoholController.numberValue / 100;
    double gasoline = _gasController.numberValue / 100;
    double resultCalc = alcohol / gasoline;
    const duration = Duration(seconds: 1);

    if ((alcohol == 0.0 || gasoline == 0.0))
      return new Future.delayed(duration, () {
        setState(() {
          _message = "Ops! \n Existem campos vazios =(";
          _complementaryInfo = "Verifique se algum campo ficou em branco.";
          _isBusy = false;
          _isCompleted = true;
          _hasError = true;
        });
      });

    setState(() {
      _color = Colors.deepPurpleAccent;
      _isCompleted = false;
      _isBusy = true;
    });

    // just to feels like a server loading the result =)
    return new Future.delayed(duration, () {
      setState(() {
        _message = (resultCalc >= 0.7)
            ? "Compensa utilizar Gasolina"
            : "Compensa utilizar Álcool";

        _complementaryInfo =
            "${(resultCalc * 100).toStringAsFixed(0)}% é o valor do álcool em relação à gasolina.";

        _isBusy = false;
        _isCompleted = true;
        _hasError = false;
      });
    });
  }

  resetForm() {
    setState(() {
      // If error, keep form values for user complete form
      if (!_hasError) {
        _alcoholController = new MoneyMaskedTextController(
            decimalSeparator: ',', thousandSeparator: '.');
        _gasController = new MoneyMaskedTextController(
            decimalSeparator: ',', thousandSeparator: '.');
      }

      _isBusy = false;
      _isCompleted = false;
      _color = Colors.deepPurple;
    });
  }
}
