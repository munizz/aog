import 'package:flutter/material.dart';
import '../components/loading-button.dart';

class Success extends StatelessWidget {
  var message = ""; // result of calc (Alcohol or Gas)
  var complementaryInfo = "";
  bool hasError;
  Function onResetForm; // to reset form values

  Success({
    @required this.message,
    @required this.complementaryInfo,
    @required this.onResetForm,
    @required this.hasError,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(30),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.8),
        borderRadius: BorderRadius.circular(25),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Text(
            message,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontFamily: "Big Shoulders Display",
                fontSize: 40),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            complementaryInfo,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontFamily: "Big Shoulders Display",
                fontSize: 20),
          ),
          SizedBox(height: 30),
          LoadingButton(
            fun: onResetForm,
            text: hasError ? 'Voltar' : 'Calcular Novamente',
            isBusy: false,
            invert: true,
          )
        ],
      ),
    );
  }
}
