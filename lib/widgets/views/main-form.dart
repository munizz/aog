import 'package:aog/widgets/components/form-input.dart';
import "package:aog/widgets/components/loading-button.dart";
import "package:flutter/material.dart";
import "package:flutter_masked_text/flutter_masked_text.dart";

class MainForm extends StatelessWidget {
  var gasController = new MoneyMaskedTextController();
  var alcoholController = new MoneyMaskedTextController();
  var isBusy = false;
  Function onSubmit;

  MainForm({
    @required this.gasController,
    @required this.alcoholController,
    @required this.isBusy,
    @required this.onSubmit,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: 40,
            right: 40,
          ),
          child: FormInput(controller: gasController, label: "Gasolina"),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 40,
            right: 40,
          ),
          child: FormInput(controller: alcoholController, label: "Álcool"),
        ),
        LoadingButton(
          fun: onSubmit,
          text: 'Calcular',
          isBusy: isBusy,
          invert: false,
        )
      ],
    );
  }
}
