import 'package:flutter/material.dart';

class LoadingButton extends StatelessWidget {
  var isBusy = false;
  var invert = false;
  Function fun;
  var text = '';

  LoadingButton({
    @required this.isBusy,
    @required this.invert,
    @required this.fun,
    @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return isBusy
        ? Container(
            alignment: Alignment.center,
            height: 50,
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          )
        : Container(
            height: 37,
            width: double.infinity,
            margin: EdgeInsets.all(30),
            decoration: BoxDecoration(
              color: invert ? Theme.of(context).primaryColor : Colors.white,
              borderRadius: BorderRadius.circular(60),
            ),
            child: FlatButton(
              child: Text(
                text,
                style: TextStyle(
                    color:
                        invert ? Colors.white : Theme.of(context).primaryColor,
                    fontFamily: "Big Shoulders Display",
                    fontSize: 25),
              ),
              onPressed: fun,
            ),
          );
  }
}
