import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 60,
        ),
        Image.asset(
          'assets/images/icon.png',
          height: 100,
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          "O que compensa mais hoje?",
          style: TextStyle(
            color: Colors.white,
            fontFamily: "Big Shoulders Display",
            fontSize: 25,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
}
