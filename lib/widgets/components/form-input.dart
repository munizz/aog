import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class FormInput extends StatelessWidget {
  var label = "";
  var controller = new MoneyMaskedTextController();

  FormInput({@required this.label, @required this.controller});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 100,
          alignment: Alignment.centerRight,
          child: Text(
            label,
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Big Shoulders Display",
                fontSize: 35),
          ),
        ),
        Expanded(
          child: TextFormField(
            controller: controller,
            keyboardType: TextInputType.number,
            textDirection: TextDirection.rtl,
            style: TextStyle(
                color: Colors.white,
                fontSize: 45,
                fontFamily: "Big Shoulders Display"),
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              border: InputBorder.none,
            ),
            inputFormatters: [
              new LengthLimitingTextInputFormatter(5),
            ],
            initialValue: null,
          ),
        )
      ],
    );
  }
}
